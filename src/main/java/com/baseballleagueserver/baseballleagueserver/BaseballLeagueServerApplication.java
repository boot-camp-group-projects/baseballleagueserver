package com.baseballleagueserver.baseballleagueserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BaseballLeagueServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(BaseballLeagueServerApplication.class, args);
    }

}

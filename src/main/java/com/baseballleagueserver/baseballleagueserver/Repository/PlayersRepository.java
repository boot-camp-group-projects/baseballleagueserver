package com.baseballleagueserver.baseballleagueserver.Repository;
import com.baseballleagueserver.baseballleagueserver.Model.PlayersModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayersRepository extends CrudRepository<PlayersModel,Long> {
}


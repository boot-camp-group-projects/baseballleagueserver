package com.baseballleagueserver.baseballleagueserver.Model;


import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.Nullable;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class PlayersModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "position") @Nullable
    private String address;
    @Column(name = "picture")
    private String zipcode;
    @Column(name = "phone")
    private String email;
    @Column(name = "description")
    private String phone;

}

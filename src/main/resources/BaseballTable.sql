CREATE TABLE teams (
                             id INT NOT NULL AUTO_INCREMENT,
                             name VARCHAR(255),
                             PRIMARY KEY (id)
);

CREATE TABLE players (
                         id INT NOT NULL AUTO_INCREMENT,
                         name VARCHAR(255),
                         position VARCHAR(255),
                         picture VARCHAR(10),
                         address VARCHAR(255),
                         phone VARCHAR(255),
                         description VARCHAR(255),
                         team_id INT,
                         PRIMARY KEY (id),
                         CONSTRAINT team1234 FOREIGN KEY (team_id)
                          REFERENCES teams(id)
);


CREATE TABLE parents (
                          id INT  NOT NULL AUTO_INCREMENT,
                          name VARCHAR(255),
                          picture VARCHAR(255),
                          phone VARCHAR(255),
                          address VARCHAR(255),
                          players VARCHAR(255),
                          volunteerhours INT,
                          player_id INT,
                          PRIMARY KEY (id),
                          CONSTRAINT player1234 FOREIGN KEY (player_id)
                            REFERENCES players(id)
);



CREATE TABLE batting(
                       id INT NOT NULL AUTO_INCREMENT,
                       atbats INT,
                       hits INT,
                       doubles INT,
                       triples INT,
                       homeruns INT,
                       runs INT,
                       walks INT,
                       strikeouts INT,
                       player_id INT,
                       team_id INT,
                       PRIMARY KEY (id),
                       CONSTRAINT team12345 FOREIGN KEY (team_id)
                         REFERENCES teams(id),
                       CONSTRAINT player12345 FOREIGN KEY (player_id)
                         REFERENCES players(id)
);

CREATE TABLE pitching(
                           id INT NOT NULL AUTO_INCREMENT,
                           wins INT,
                           losses INT,
                           hits INT,
                           walks INT,
                           strikeouts INT,
                           runs INT,
                           pitchcount INT,
                           innings INT,
                           team_id INT,
                           player_id INT,
                           PRIMARY KEY (id),
                           CONSTRAINT team123456 FOREIGN KEY (team_id)
                             REFERENCES teams(id),
                           CONSTRAINT player123456 FOREIGN KEY (player_id)
                             REFERENCES players(id)
);

CREATE TABLE location (
                        id INT NOT NULL AUTO_INCREMENT,
                        name VARCHAR(255),
                        address VARCHAR(255),
                        city VARCHAR(255),
                        state VARCHAR(255),
                        zip VARCHAR(255),
                        PRIMARY KEY (id)
);

CREATE TABLE schedules (
                        id INT NOT NULL AUTO_INCREMENT,
                        day DATE,
                        hour TIME,
                        location_id INT,
                        team_id INT,
                        PRIMARY KEY (id),
                        CONSTRAINT location123 FOREIGN KEY (location_id)
                          REFERENCES location(id),
                        CONSTRAINT team123 FOREIGN KEY (team_id)
                          REFERENCES teams(id)
);



CREATE TABLE leaguelevel(
                          id INT NOT NULL AUTO_INCREMENT,
                          name VARCHAR(255),
                          age VARCHAR(255),
                          PRIMARY KEY (id)
);

CREATE TABLE coaches(
                      id INT NOT NULL AUTO_INCREMENT,
                      name VARCHAR(255),
                      address VARCHAR(255),
                      phone VARCHAR(255),
                      team_id INT,
                      wins INT,
                      losses INT,
                      background DATE,
                      trainings VARCHAR(255),
                      PRIMARY KEY (id),
                      CONSTRAINT team12 FOREIGN KEY (team_id)
                        REFERENCES teams(id)
);

CREATE TABLE teamrecord (
                      id INT NOT NULL AUTO_INCREMENT,
                      wins INT,
                      losses INT,
                      pitching_runs INT,
                      team_id INT,
                      batting_runs INT,
                      PRIMARY KEY (id),
                      CONSTRAINT team124 FOREIGN KEY (team_id)
                        REFERENCES teams(id),
                      CONSTRAINT pitching124 FOREIGN KEY (pitching_runs)
                        REFERENCES pitching(id),
                      CONSTRAINT batting124 FOREIGN KEY (batting_runs)
                        REFERENCES batting(id)
);


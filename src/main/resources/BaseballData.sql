INSERT INTO teams (id, name)
VALUES (1,'Tigers'),( 2,'Braves'),(3,'Dodgers'),(4,'Mets'),(5, 'Athletics'),(6,'Giants'),(7,'Cubs'),(8,'Red Sox')
      ,(9,'Indians'),(10, 'Royals'),(11,'Rangers'),(12,'Reds'),(13,'Angels'),(14,'Rockies'),(15,'Twins');

INSERT players ( id,name,position,picture,address,phone,description,team_id)
VALUES (1,'John','','','','','',1),
       (2,'Joe','','','','','',2),
       (3,'Jane','','','','','',3),
       (4,'Jim','','','','','',1),
       (5,'Jose','','','','','',2),
       (6,'Juan','','','','','',3),
       (7,'Mike','','','','','',1),
       (8,'Suzie','','','','','',2),
       (9,'Brian','','','','','',3),
       (10,'Carter','','','','','',1),
       (11,'Ryder','','','','','',2),
       (12,'Michael','','','','','',3);

INSERT parents ( id,name,picture,phone,address,players,volunteerhours,player_id)
VALUES (1,'John','picture','phone','address','players','10','1');

INSERT coaches(id,name,address,phone,team_id,wins,losses,background,trainings)
VALUES (1,'John','address','phone',1,5,10,19990303,'trainings')
       ,(2,'Jim','address','phone',2,20,5,19990303,'trainings')
       ,(3,'Joe','address','phone',3,15,7,19990303,'trainings')
;

INSERT leaguelevel(id,name,age)
VALUES (1,'High School','High School')
       ,(2,'Junior High', 'Junior High')
     ,(3, 'Majors Baseball','5th/6th Grade')
     ,(4, 'Minors Baseball','3rd/4th Grade')
     ,(5, 'Pee Wee Baseball','1st/2nd Grade')
     ,(6, 'Rookieball','Kindergarten')
     ,(7, 'Rookieball','3-4')
;

INSERT teamrecord (id,wins,losses,team_id)
VALUES (1,'5','3',1)
       ,(2,'4','4',1)
       ,(3,'3','5',1)
       ;

INSERT location (id,name,address,city,state,zip)
VALUES (1,'Sports Complex north field','address','Ogden','Utah','84404')
       ,(2,'Sports Complex south field','address','Ogden','Utah','84404')
       ,(3,'Sports Complex east field','address','Ogden','Utah','84404')
       ,(4,'Sports Complex west field','address','Ogden','Utah','84404')
       ,(5,'Ellis Park','address','Layton','Utah','84041')
;

INSERT schedules (id,day,hour,location_id,team_id)
VALUES (1,'20190303','05:00:00',1,1)
      ,(2,20190303,'06:00:00',2,2)
      ,(3,20190303,'07:00:00',3,3)
;

INSERT batting(id,atbats,hits,doubles,triples,homeruns,walks,strikeouts,player_id,team_id,runs)
VALUES (1,20,5,2,1,0,2,5,1,1,11)
      ,(2,20,5,2,1,0,2,5,2,2,12)
      ,(3,20,5,2,1,0,2,5,3,3,5)
;

INSERT pitching(id,wins,losses,hits,walks,strikeouts,runs,pitchcount,innings,team_id,player_id)
VALUES (1,3,2,2,1,5,2,5,40,1,4)
     ,(2,3,2,2,1,5,2,5,30,1,5)
     ,(3,3,2,2,1,5,2,5,20,1,6)
;
